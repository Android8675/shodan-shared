![](https://img.shields.io/endpoint?url=https://foundryshields.com/version?url=https%3A%2F%2Fgitlab.com%2FAndroid8675%2Fshodan-shared%2F-%2Fraw%2Fmain%2Fmodule.json) - ![](https://img.shields.io/endpoint?url=https%3A%2F%2Ffoundryshields.com%2Fsystem%3FnameType%3Dfull%26url%3Dhttps%3A%2F%2Fimg.shields.io%2Fendpoint%3Furl%3Dhttps%3A%2F%2Ffoundryshields.com%2Fversion%3Furl%3Dhttps%253A%252F%252Fgitlab.com%252FAndroid8675%252Fshodan-shared%252F-%252Fraw%252Fmain%252Fmodule.json)

# My Server Compendiums

This is mainly just my server compendiums in case I want to easily move things from one world to another, test deployments of new material, etc.

I don't know if this will morph into something interesting, but you never know.

## Deployment

1. If you're reading this you should mess around with other Foundry Addons first, then come back.
2. Copy the [Manifest URL](https://gitlab.com/Android8675/shodan-shared/-/raw/main/module.json) into your Foundry Add-Ons Setup Screen in your admin menu.

## Contact Info

You can reach me on Discord @Android8675#9424, or [email me](mailto:66xakfaar@relay.firefox.com?subject=Shodan-Server%20Question) directly.
